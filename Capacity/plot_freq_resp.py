#!/usr/bin/env python
"""
plot_fft

"""

from pylab import *

rc('text', usetex=True)
rc('text', fontsize=20)
rc('axes', labelsize=22)
rc('legend', fontsize=20)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

s = ones(10)
fraction = 100.0 / 1024.0
N = 1024
M = int(fraction * N)
f = fft(s, N) + 0.2 * (randn(N) + 1j * randn(N)) / 1.4142135623730950488016887242096980785696718753769480
x_vals = r_[0:M] / 100.0
y_vals = 10*log10(abs(f[0:M]) / 10.5)
plot(x_vals, y_vals, 'k-', lw=2)
STEP = 1
A = 40
B = 54
for i in r_[A:B:STEP]:
    print i
    rect = Rectangle((x_vals[i], -14), x_vals[i + 1] - x_vals[i], y_vals[i] + 14, facecolor='#ffffff')
    gca().add_patch(rect)

i = 47
rect = Rectangle((x_vals[i], -14), x_vals[i + 1] - x_vals[i], y_vals[i] + 14, facecolor='#00ffff')
gca().add_patch(rect)

# text(x_vals[i] + 0.0, y_vals[i] + 1.6, r'100 MHz')
# arr = Arrow(x_vals[i] + 0.0, y_vals[i] + 1.5, -0.0, -1.4,lw=0.01, edgecolor='white')
# gca().add_patch(arr)
grid(True)
xlabel(r'Frequency (GHz)')
ylabel('Fiber frequency response $|H(\omega)|^2$ (dB)')
v = axis()
axis((v[0], v[1], -14, v[3]))
savefig('capacity_frequency_selective.pdf')
close()
