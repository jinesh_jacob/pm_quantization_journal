import os
import sys
import pickle
import numpy
import fnmatch
from pylab import *

def load_frequency_dictionary(filename):
    
        main_dir=os.path.realpath(os.path.dirname(sys.argv[0]))
    
 #       pickle_dir = os.path.join(main_dir, 'plots_new')

 #       capacity_data_dir = os.path.join(pickle_dir, 'capacity_data')
    
        full_file_name=os.path.join(main_dir, filename)
    
        file_open_for_read=open(full_file_name,'r')
    
        frequency_dictionary=pickle.load(file_open_for_read)
    
        file_open_for_read.close()
    
        return frequency_dictionary


def save_image(imagename):
    
        legend(numpoints=1, loc=2)
        
        ylabel(r'Rate (in Gbps)')
        
        xlabel(r'SNR (in dB)')
        
        grid(True)

        yticks([0.0,1.0e+9,2.0e+9,3.0e+9,4.0e+9,5.0e+9,6.0e+9,7.0e+9],[0,1,2,3,4,5,6,7])
        x_tick_vals = xticks()[0]
        xticks(x_tick_vals, x_tick_vals)

        
        axes=gca()
        
#        axes.set_ylim([0.0,9.0])
        
        main_dir=os.path.realpath(os.path.dirname(sys.argv[0]))
    
#        plot_dir = os.path.join(main_dir, 'plots_new')

#        capacity_plot_dir= os.path.join(plot_dir, 'capacity_plots')
    
        full_file_name=os.path.join(main_dir, imagename)

        savefig(full_file_name)
        
        close()

        return None

def convert_from_dB_to_actual_value(input_array):
    
        return 10**(input_array/10)

def calculate_outage_capacity(outage_value,input_array):
    
    input_arr_col_size=input_array.shape[1]

    outage_capacity_arr=numpy.array([])

    for col_index in range(input_arr_col_size):

        current_hist_data=input_array[:,[col_index]].flatten()

        hist_data=numpy.histogram(current_hist_data,1000)
        
        bin_count=hist_data[0]

        bin_edges=hist_data[1]

        bin_probability=bin_count.astype(float)/numpy.sum(bin_count)

        reverse_bin_probability=bin_probability[::-1]

        reverse_cum_sum=numpy.cumsum(reverse_bin_probability)

        cum_sum=reverse_cum_sum[::-1]

        outage_bin_location=0

        for index in range(len(cum_sum)):

            if (cum_sum[index] < outage_value) :

                outage_bin_location=index
    
                break

        outage_capacity_arr=numpy.hstack((outage_capacity_arr,bin_edges[outage_bin_location]))        
            
    return outage_capacity_arr
        

color={'4 bits':'g-o','5 bits':'b-^','6 bits':'m-*','7 bits':'r-+','8 bits':'y-p','Ideal mode':'c-D'}

ms = 10.0
        
markevery = 100

rc('text', usetex=True)

rc('text', fontsize=20)

rc('axes', labelsize=20)

rc('legend', fontsize='xx-small')

rc('xtick', labelsize=15)

rc('ytick', labelsize=15)


max_SNR=20.0

#noise_power=1.0 # Area under the noise PSD curve

band_width=1.0e+9

SNR_range_dB=numpy.arange(0.0,max_SNR,(max_SNR/1000))

SNR_actual=convert_from_dB_to_actual_value(SNR_range_dB)


for section in [6]:

    for PM_index in [4]:

        frequency_dictionary_filename='frequency_'+str(section)+'_sections_PM_'+str(PM_index) # changed for Rajesh

        frequency_dictionary=load_frequency_dictionary(frequency_dictionary_filename)

        frequencies=frequency_dictionary['frequencies']

        frequency_bin_size= (frequencies[1]-frequencies[0])

        del frequency_dictionary['frequencies']

        plot(SNR_range_dB,(band_width*numpy.log2(1+SNR_actual)),'k-|' , lw=2,ms=ms,markevery=markevery,markeredgecolor='black',label='Unquantized PM',markeredgewidth=1) 

        for key in sorted(frequency_dictionary.keys()):

                frequency_responses_arr=frequency_dictionary[key]

                total_capacity_arr=numpy.zeros(len(SNR_actual))

                for frequency_responses_arr_index in range(frequency_responses_arr.shape[1]):

                        current_response_dB_arr=frequency_responses_arr[:,[frequency_responses_arr_index]].flatten()

                        current_response_arr=convert_from_dB_to_actual_value(current_response_dB_arr)

                        capacity_arr=numpy.array([])

                        for SNR in SNR_actual:

                            inst_capacity=frequency_bin_size*numpy.log2(1+(SNR*current_response_arr))

                            capacity_arr= numpy.hstack((capacity_arr,numpy.sum(inst_capacity)))

                        total_capacity_arr=numpy.vstack((total_capacity_arr,capacity_arr))        

                average_capacity= numpy.mean(total_capacity_arr,axis=0) 

                plot(SNR_range_dB, average_capacity, color[key], lw=2,ms=ms,markevery=markevery,markeredgecolor='black', label=key, markeredgewidth=1) 




        save_image('average_capacity_'+str(section)+'_sections_PM'+str(PM_index)+'.pdf')
