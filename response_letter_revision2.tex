\documentclass{letter}
\usepackage[left=1in,twoside=false]{geometry}
\usepackage[cmex10]{amsmath}
\usepackage{amssymb}
\signature{Jinesh C Jacob}
\address{From \\
Jinesh C Jacob\\
Department of Electrical  Engineering\\
Indian Institute of Technology Bombay, Mumbai\\
India\\
E-mail: jinesh\_jacob@iitb.ac.in
\date{\today}
}

\begin{document}
\begin{letter}{To\\
Prof. Naofal Al-Dhahir\\
Editor-in-Chief\\
IEEE Transactions on Communications}
\vspace{0.5in}


\opening{Dear Prof. Al-Dhahir,} 

Subject: \textbf{Revised submission of manuscript title ``Quantization and Feedback of Principal Modes for Dispersion   Mitigation and Multiplexing in Multimode Fibers'' (Paper Number: TCOM-TPS-16-0216.R1).}

Authors: Jinesh C Jacob, Rajesh Kumar Mishra, Kumar Appaiah\\
Email: jinesh\_jacob@iitb.ac.in


Thank you for arranging the prompt reviews of our original manuscript
submitted to the IEEE Transactions on Communications, and providing us
an opportunity to resubmit this manuscript with modifications
addressing the issues raised during the reviews. I would like to thank
the reviewers for a careful reading of the manuscript, as well as for
the insightful points that they have raised regarding various technical
aspects of the manuscript. Based on these reviews, we have made
extensive additions and revisions to our submission to address the
reviewers' concerns. In particular:

\begin{enumerate}
\item We have attempted to fill the gaps on the complexity of the
  algorithms we have suggested (LBG and Grassmannian line packing) and
  their relevance to the communication paradigm.
\item We have now included more details on the feedback assumptions in the paper.
\item We have added a BER plot to provide a representative picture of
  the impact of principal mode quantization, in addition to the
  achievable rate plots.
\end{enumerate}

We have done our best to understand and address the reviewers'
concerns. We hope that you will consider our improved manuscript for
publication in the IEEE Transactions on Communications. Please do not hesitate to contact me if I may be of any assistance; I
can be reached by e-mail at jinesh\_jacob@iitb.ac.in. Thank you again
for your consideration of this manuscript.

Thank you.
\vspace{0.1in}

Sincerely,

Jinesh C Jacob



\end{letter}

\newpage

\textbf{Editor comments} 

\textbf{Based on the reviewers' comments and my own
reading of your manuscript, I recommend that you update your paper and
submit a revised version for further processing. In addition to the
comments of the reviewers, I would like you to expand on the
applicability of the proposed approach (e.g., fiber length,
computational complexity requirements), given the 0.25 ms allowable
delay.}

Thank you for your consideration. We have handled the aspects related
to complexity to the best of our ability in the current manuscript. We
request you to refer to the response to the first query of Reviewer 1
below for our explanation. We have also expanded the 0.25 ms feedback
assumption in the Introduction as follows:

\textit{In [12], it has been demonstrated that the channel evolution frequency of few-
mode fiber channels is around 4 kHz. Thus, the principal modes of the fiber channel may be
assumed to be constant for about 0.25 ms, thus limiting effective round trip length of the link
at around 50 km. Moreover, the utility of feedback over links of a few kilometers has also been
effectively demonstrated in [10]. In this paper, we restrict our study to three-mode fiber links
with length less than 10 km.}



\textbf{Reviewer 1}\\

\textbf{Comment 1: It is mentioned that the codebook generation is one-time process. However, I couldn't understand how the change in the mode coupling dynamics and characteristics effect the generated codebooks. Please explain how often the codebook generation should be implemented in detail..}

Thank you for pointing this out. In the section IIB (p.14),  We have discussed about the different factors effecting on the codebook generation which is given below.

\textit{``As discussed in the introduction, mode coupling in MMFs can
  happen in two forms, viz.  weak mode coupling and strong mode
  coupling. If the modes are weakly coupled, it can be seen that
  randomly distributed PMs will form the clouds around each ideal mode
  in $\mathbb{C}^{2M}$ space when many random realizations of given length of the
  fiber are taken into account, i.e the PMs are highly correlated
  within each PM cluster. In the above scenario, we can use the LBG
  algorithm to generate a codebook for PM quantization. Each fiber
  realization has been generated using the multisection model
  discussed in the Section II-A1 and splice model in discussed in
  Section II-A2.  In the lossless case, the statistical properties of
  each PM solely depend on the statistical properties of $\kappa$ (related to
  bend in each section) and $\theta$ (rotation between the sections). It also
  depends on the $\phi$ (rotation between splice sections) in the MDL
  case. As long as the statistical properties (variance) of the above
  parameters remains constant over time, the distribution of each PM
  will also be stationary in time. Thus, for the stationary case, we
  only need to generate a single instance of the LBG codebook for a
  given weakly coupled fiber. Similarly, it has been shown that PMs in
  strongly coupled fibers are uniformly distributed on the surface of
  the unit sphere in $\mathbb{C}^{2M}$ space irrespective of the statistical
  properties of the fiber parameters, as established below. Thus, we
  can use an off-the-shelf Grassmannian codebook for PM quantization
  for the given number of modes 2M and the codebook remains fixed over
  the time.  ''}
 
\textbf{Comment 2: I think the Authors should discuss how the complexity of codebook generation depends on various parameters, e.g. b. Even though the algorithm might be run very infrequently, a discussion about associated complexity will be helpful.}

Thank you for notifying us of this. We have expanded the discussion on
complexity in section II-B (p.14) with the text below.

\textit{``The complexity of the LBG algorithm is discussed in [29], where it has been shown that the
total computational time for the LBG algorithm in d-dimensional space with n training vectors,
k quantization levels and is $O(n^{dk + 1}\log n)$, which simplifies
to $O(n^{6k + 1}\log n)$ for the 6 mode few-mode
fiber case considered in this paper. In the Grassmannian codebook case, the search complexity is
exponential in the number of quantization elements k, although, in this case, the codebooks can
be read off a table like [30], since the same codebook works for all 6
mode strongly coupled
fibers. Although this codebook generation does impose a high computation requirement, since
this is a one-time process for a given fiber link that does not have to be repeated during operation,
the complexity requirements do not play a part in the communication paradigm.''}

\textbf{Comment 3: The practical outage probabilities should be much smaller than 0.05. A better parameter choice can be 0.0001. I suggest revising the Fig. 8 to improve the impact of the paper.}

The reviewer is correct in pointing this out. We have added new plots
with  the outage probability of 0.001. Since the low value of outage
probability (0.0001) demands large number of random fiber realizations
and matrix model of each fiber realization involves many matrix
multiplications, it requires large amount of time to conduct the
simulations; we have thus restricted ourselves to these outage values.


\textbf{Reviewer 2}\\

\textbf{Comment 4: How does the technique in [7] for realizing PM transmission compare with the feedback-based PM realization scheme the authors propose in this paper? Are there any benefits of realizing PMs through feedback as opposed to what is done in [7]? Some remarks regarding this in the introduction would be helpful.}

Thank you for the remark. The consideration in [7] and our work should
be viewed as complementary. In [7], it has been shown that, by
measuring and transmitting using the first order PMs, effective
dispersion-free channel responses can be realized. Our work considers
the aspect of efficiently compressing and communicating the PM
information so that the transmitter can utilize it. We have added the
difference between the approaches in [7] and our approach which is
given below

\textit{``In [7], transmitter and receiver are
co-located and the aspect of efficiently conveying information about the PMs
measured at the receiver side to the transmitter side is not considered.''}


\textbf{Reviewer 3}\\

\textbf{Comment 5: Although many of the comments that were sent for the first version of the paper were addressed by the authors, system level implications are still missing in order to better understand the impact of quantizing principal modes on a short-distance MMF transmission. Adding metrics as the ergodic and the outage capacity goes in this direction. However, the simulation parameters are not in line with the state-of-the art works in optical multi-mode communications (bandwidth up to dozens of GHz instead of 1 GHz, and lower outage probabilities are usually considered). BER curves versus SNR would be a good way to complement the work and show the impact of quantization. Given that feedback solutions introduce delays and costs (even if the learning operation can be done at a lower frequency, on a millisecond scale as stated in the paper), the proposed solution and study of quantization impact should be more complete and take into consideration at least white Gaussian noise at the receiver side. Furthermore, several parts of the paper can be shortened further or be placed in other sections (for instance, the last paragraphs of the introduction on p. 5 can be placed in the system model).}

Thank you for the comment. We agree that a representative plot that
provides a realistic picture would be useful. We have added a BER plot
in Section III-B (p.25 Figure 11) to handle this. In addition, we have
shortened the introduction by moving the concerned paragraphs to
Section II-B in the System Model part to convey the information on
the precise quantization and feedback paradigm considered.

\textbf{Comment 6: Concerning the added capacity curves, a surprising result is observed when comparing the performance of the ideal modes to the one of the non-quantized principal modes in presence of MDL (figures 9 and 10). Why would the Shannon capacity be different between the two, because it should be related to the singular values of the MIMO channel depicting the MMF-based link?}

Thank you for the remark. In this paper, we calculate the rate
achievable through a single quantized PM channel for a given number of
quantization levels. This is different from the maximum achievable
rate (Shannon capacity) using all spatial channels together. In this
paper, the spatial channels available to us are quantized PM
channels. Thus, we measure the achievable rate of a frequency
selective channel (the PM channels become frequency selective due to
imperfect quantization) to compare the quantization performance of
PMs. In our case, we assume that the transmitter knows only the label
of the quantized PM vector, and that it does not have any information
about the actual PMs and and the frequency response of the
channel. Thus, the transmitter transmits all the input power equally
in each frequency bins and it does not perform any optimization in
input power allocation. We have now indicated this clearly in the
manuscript with the following addition:

\textit{It must be noted that the achievable
rate is calculated assuming that all the transmit energy is launched
into the quantized PM, as opposed to performing an optimal power
allocation across the modes of the fiber. This restriction is in place
since we assume that the only feedback from the receiver is the
quantized PM vector, as opposed to complete the frequency response of
the fiber channel.}

\end{document}
