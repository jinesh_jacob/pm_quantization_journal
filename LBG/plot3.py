#!/usr/bin/python

from scipy import *
import scipy.io as sio
import numpy
from pylab import *
import sys
import itertools

rc('text', usetex=True)
rc('text', fontsize=20)
rc('axes', labelsize=20)
rc('legend', fontsize='xx-small')
rc('xtick', labelsize=15)
rc('ytick', labelsize=15)

# def read_file(filename):
#     v = numpy.loadtxt(filename)
#     return transpose(v)

# z = read_file('benefit.txt')
# z[1] = (z[1] / max(z[1]) + 1.0)*0.98
# z[2] = (z[2] / max(z[2]) + 1.0)

Data1_grass=sio.loadmat('plot3_grass.mat')
Data11_grass=10*numpy.log10(numpy.array(numpy.mat(Data1_grass['F'])))
Data1_grass_array=numpy.array(Data11_grass).flatten()

Data1_lbg=sio.loadmat('plot3_lbg.mat')
Data11_lbg=10*numpy.log10(numpy.array(numpy.mat(Data1_lbg['G'])))
Data1_lbg_array=numpy.array(Data11_lbg).flatten()



var_x=sio.loadmat('var3.mat')
var_x_array=numpy.array(var_x['w']).flatten()
ms = 10.0
markevery = 10

plot(var_x_array, np.zeros(101), 'y-s', lw=2,ms=ms,markevery=markevery,label='Unquantized PM',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_grass_array, 'b-^', lw=2,ms=ms,markevery=markevery,label='Grassmanian',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_lbg_array, 'g-o', lw=2,ms=ms,markevery=markevery,label='LBG',markeredgecolor='black',markeredgewidth=1)

legend(numpoints=1, loc=1,fontsize = 'xx-small')
ylabel(r'Magnitude response (dB)')
xlabel(r'Normalised frequency')
axis([0.0,1,-4,1])
grid(True)
savefig('icc_plot3.pdf')
close()

