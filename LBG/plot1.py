#!/usr/bin/python

from scipy import *
import scipy.io as sio
import numpy
from pylab import *
import sys
import itertools

rc('text', usetex=True)
rc('text', fontsize=20)
rc('axes', labelsize=20)
rc('legend', fontsize='xx-small')
rc('xtick', labelsize=15)
rc('ytick', labelsize=15)

# def read_file(filename):
#     v = numpy.loadtxt(filename)
#     return transpose(v)

# z = read_file('benefit.txt')
# z[1] = (z[1] / max(z[1]) + 1.0)*0.98
# z[2] = (z[2] / max(z[2]) + 1.0)

Data1_100=sio.loadmat('plot1_100.mat')
Data11_100=10*numpy.log10(numpy.array(numpy.mat(Data1_100['F5'])))
Data1_100_array=numpy.array(Data11_100).flatten()

Data1_300=sio.loadmat('plot1_300.mat')
Data11_300=10*numpy.log10(numpy.array(numpy.mat(Data1_300['G5'])))
Data1_300_array=numpy.array(Data11_300).flatten()

Data1_600=sio.loadmat('plot1_600.mat')
Data11_600=10*numpy.log10(numpy.array(numpy.mat(Data1_600['H5'])))
Data1_600_array=numpy.array(Data11_600).flatten()

Data1_800=sio.loadmat('plot1_800.mat')
Data11_800=10*numpy.log10(numpy.array(numpy.mat(Data1_800['I5'])))
Data1_800_array=numpy.array(Data11_800).flatten()

Data1_1000=sio.loadmat('plot1_1000.mat')
Data11_1000=10*numpy.log10(numpy.array(numpy.mat(Data1_1000['J5'])))
Data1_1000_array=numpy.array(Data11_1000).flatten()



var_x=sio.loadmat('var1.mat')
var_x_array=numpy.array(var_x['omeg']).flatten()*1e-9
ms = 10.0
markevery = 10

plot(var_x_array, np.zeros(101), 'y-s', lw=2,ms=ms,markevery=markevery,label='Unquantized PM',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_100_array, 'b-^', lw=2,ms=ms,markevery=markevery,label='100m fiber',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_300_array, 'g-o', lw=2,ms=ms,markevery=markevery,label='300m fiber',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_600_array, 'c-D', lw=2,ms=ms,markevery=markevery,label='500m fiber',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_800_array, 'm-*', lw=2,ms=ms,markevery=markevery,label='800m fiber',markeredgecolor='black',markeredgewidth=1)
plot(var_x_array, Data1_1000_array, 'r-+', lw=2,ms=ms,markevery=markevery,label='1000m fiber',markeredgecolor='black',markeredgewidth=1)

legend(numpoints=1, loc=1,fontsize = 'xx-small')
ylabel(r'Magnitude response (dB)')
xlabel(r'Frequency (GHz)')
axis([0.0,10,-1.5,1])
grid(True)
savefig('icc_plot1.svg')
close()

