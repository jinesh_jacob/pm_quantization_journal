#!/usr/bin/python

from scipy import *
import scipy.io as sio
import numpy
from pylab import *
import sys
import itertools
from math import *
rc('text', usetex=True)
rc('text', fontsize=20)
rc('axes', labelsize=20)
rc('legend', fontsize='xx-small')
rc('xtick', labelsize=15)
rc('ytick', labelsize=15)

# def read_file(filename):
#     v = numpy.loadtxt(filename)
#     return transpose(v)

# z = read_file('benefit.txt')
# z[1] = (z[1] / max(z[1]) + 1.0)*0.98
# z[2] = (z[2] / max(z[2]) + 1.0)

Data_Grass=sio.loadmat('Grass_strong.mat')
Grass_matrix=10*numpy.log10(list(itertools.chain.from_iterable(Data_Grass['E'])))
Data_LBG=sio.loadmat('LBG_strong.mat')
LBG_matrix=10*numpy.log10(list(itertools.chain.from_iterable(Data_LBG['E'])))
vector_x=numpy.linspace(0.0, 1.0, num=1024, endpoint=False)


ms = 10.0
lw = 2
markevery = 100
plot(vector_x[0:512], np.zeros(512), 'y-s', lw=lw,ms=ms,markevery=markevery,label='Unquantized PM',markeredgecolor='black',markeredgewidth=1)
plot(vector_x[0:512], Grass_matrix[0:512], 'm-^', lw=lw,ms=ms,markevery=markevery,label='Grassmannian in Strong Coupling',markeredgecolor='black',markeredgewidth=1)
plot(vector_x[0:512], LBG_matrix[0:512], 'b-o', lw=lw,ms=ms,markevery=markevery,label='LBG in Strong Coupling',markeredgecolor='black',markeredgewidth=1)
#plot(vector_x[0:512], vector_3bit[0:512], 'r-*', lw=lw,ms=ms,markevery=markevery,label='3 bit quantized PM',markeredgecolor='black',markeredgewidth=1)
#plot(vector_x[0:512], vector_ideal[0:512], 'k', lw=lw,ms=ms,markevery=markevery,label='Ideal Mode',markeredgecolor='black',markeredgewidth=1) 
legend(numpoints=1, loc=1,fontsize = 'xx-small')
ylabel(r'Magnitude response (dB)')
xlabel(r'Normalized frequency')
axis([0.0,0.5,0.-5,2])
grid(True)
savefig('grassmanian_lbg.svg')
close()
