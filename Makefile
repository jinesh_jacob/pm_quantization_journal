FILENAME = journal
COVER_LETTER=cover_letter
RESPONSE_LETTER=response_letter
SECOND_RESPONSE_LETTER=response_letter_revision2

all: $(FILENAME).pdf $(COVER_LETTER).pdf $(RESPONSE_LETTER).pdf $(SECOND_RESPONSE_LETTER).pdf

$(COVER_LETTER).pdf: $(COVER_LETTER).tex
	pdflatex $(COVER_LETTER)

$(RESPONSE_LETTER).pdf: $(RESPONSE_LETTER).tex
	pdflatex $(RESPONSE_LETTER)

$(SECOND_RESPONSE_LETTER).pdf: $(SECOND_RESPONSE_LETTER).tex
	pdflatex $(SECOND_RESPONSE_LETTER)


$(FILENAME).pdf: $(FILENAME).tex references.bib
	pdflatex $(FILENAME).tex && \
	bibtex $(FILENAME) && \
	pdflatex $(FILENAME).tex && \
	pdflatex $(FILENAME).tex
	pdflatex $(COVER_LETTER).tex

.PHONY: clean
clean:
	$(RM) $(FILENAME).pdf *.aux *.bbl *.blg *.log $(COVER_LETTER).pdf $(RESPONSE_LETTER).pdf
